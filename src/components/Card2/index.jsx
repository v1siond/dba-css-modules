import React, { useState } from 'react';
import { dbaTitle, h2 } from '../../styles/modules/pages/home.module.scss';
import { card2, stateChange } from '../../styles/modules/pages/another.module.scss';

import bind from 'classnames/bind';

const cn = bind.bind({card2, stateChange, h2, dbaTitle});

const Card2 = () => {

  const [stateChanged, setStateChanged] = useState(false)

  return (
    <div onClick={() => setStateChanged(!stateChanged)} className={cn(card2, { stateChange: stateChanged })}>
      <h2 className={cn(dbaTitle, { h2: stateChanged })}>Hello World 2</h2>
    </div>
  )
};

export default Card2;
