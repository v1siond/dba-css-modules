import React from 'react';
import { container, dbaCard, dbaTitle, h1 } from '../../styles/modules/pages/home.module.scss';
import Card2 from '../Card2';

const Home = () => (
  <div className={container}>
    <div className={dbaCard}>
      <h1 className={`${dbaTitle} ${h1}`}>Hello World</h1>
    </div>
    <Card2 />
  </div>
);

export default Home;
