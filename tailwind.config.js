const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === "production",
    content: [
      './**/*.js',
      './**/*.scss',
      './**/*.jsx',
      './src/**/*.scss',
      './src/**/*.js',
      './src/**/*.jsx',
    ]
  },
  darkMode: false,
  theme: {
    extend: {
      colors: {
        ...defaultTheme.colors,
        yellow: {
          ...defaultTheme.colors.yellow,
          'dba': '#FFC933'
        },
        purple: {
          ...defaultTheme.purple,
          'dba': '#9747ff'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
